module mux_2_1_1_bit_logic (x, y, s, m);
input x, y, s;
output m;
	
	assign m = (~s & x) | (s & y);
endmodule

module zad1 (SW, KEY, LEDR);
input [1:0] SW;
input [0:0] KEY;
output [0:0] LEDR;
	mux_2_1_1_bit_logic logic(SW[0], SW[1], KEY[0], LEDR[0]);
endmodule