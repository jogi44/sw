module mux_2_1_1_bit_logic (x, y, s, m);
input x, y, s;
output m;
	
	assign m = (~s & x) | (s & y);
endmodule

module mux_4_1_1_bit_logic (u, v, w, x, S1, S2, y);
	input u, v, w, x;
	input S1, S2;
	output y;
	wire uv, wx;
	
	mux_2_1_1_bit_logic mux1(u, v, S1, uv); // wykorzystywany mux 2 x 1 z zad 1
	mux_2_1_1_bit_logic mux2(w, x, S1, wx);
	mux_2_1_1_bit_logic mux3(uv, wx, S2, y);
endmodule

module zad3(SW, KEY, LEDR);
input [3:0] SW;
input [1:0] KEY;
output [0:0] LEDR;
	mux_4_1_1_bit_logic logic(SW[0], SW[1], SW[2], SW[3], KEY[1], KEY[0], LEDR[0]);
endmodule
