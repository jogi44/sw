module zad5 (
	input [3:0] SW,
	output reg [0:6] H0,H1);
	always @(SW)
		case (SW[3:0])
			0: begin
				H0 = 7'b0000001;
				H1 = 7'b0000001;
				end
				
			1: begin
				H0 = 7'b1001111;
				H1 = 7'b0000001;
				end
				
			2: begin
				H0 = 7'b0010010;
				H1 = 7'b0000001;
				end
				
			3: begin
				H0 = 7'b0000110;
				H1 = 7'b0000001;
				end
				
			4: begin
				H0 = 7'b1001100;
				H1 = 7'b0000001;
				end
				
			5: begin
				H0 = 7'b0100100;
				H1 = 7'b0000001;
				end
				
			6: begin
				H0 = 7'b0100000;
				H1 = 7'b0000001;
				end
				
			7: begin
				H0 = 7'b0001111;
				H1 = 7'b0000001;
				end
				
			8: begin
				H0 = 7'b0000000;
				H1 = 7'b0000001;
				end
				
			9: begin
				H0 = 7'b0000100;
				H1 = 7'b0000001;
				end
				
			10: begin
				H0 = 7'b0001000;
				H1 = 7'b1001111;
				end
				
			11: begin
				H0 = 7'b1001111;
				H1 = 7'b1001111;
				end
				
			12: begin
				H0 = 7'b0010010;
				H1 = 7'b1001111;
				end
				
			13: begin
				H0 = 7'b0000110;
				H1 = 7'b1001111;
				end
				
			14: begin
				H0 = 7'b1001100;
				H1 = 7'b1001111;
				end
				
			15: begin
				H0 = 7'b0100100;
				H1 = 7'b1001111;
				end
				
			default: begin
				H0 = 7'b1111111;
				H1 = 7'b1111111;
				end
		endcase
endmodule