Symulacja dotyczy dw�ch wy�wietlaczy 7- segmentowych.
Szczeg�y s� w wyk�adzie (Uk�ady wej�cia / wyj�cia).

Zaprogramowanych jest 8 przycisk�w SW[0:7].
Przyciski SW[0:3] wy�witlaj� liczbe szestnastkow�, podobnie jak przyciski SW[4:7].

Aby wy�wietli� liczb� 3 na wy�wietlaczu trzeba wcisn�� przycisk SW[0] oraz SW[1] co daje 0011 w uk�adzie binarnym czyli 3.
Wida� to na symulacji podane s� jedynki na przycisk SW[0] oraz SW[1] w wyniku tego wida�,
�e wy�wietlacz HEX0 wy�wietla liczb� poprzez zapalenie odpowiednich segment�w HEX[0],HEX[1],HEX[2],HEX[3] oraz HEX[6]

WA�NE - nale�y zwr�ci� uwage �e podane do wy�wietlenia segmenty przyjmuj� warto�� 0, dzia�a to odwrotnie poniewa�
s� to wy�wietlacze ze wsp�ln� anod�, kt�re dzia�aj� odwrotnie ni� wy�wietlacze ze wsp�ln� katod�.
